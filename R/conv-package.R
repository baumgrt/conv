#' @name        conv-package
#' @title       Convolution and Convolution Powers of Probability Vectors
#' @author      Benjamin Baumgartner \email{benjamin@@baumgrt.com}
#' @keywords    math
#' @aliases     conv-package
#'
#' 
#' @description
#' Uses the Fast Fourier Transform to compute convolutions of two or more
#' probability vectors or a convolution power of such a vector.
#' 
#'
#' @details
#' This package contains the functions \code{\link{conv}} and
#' \code{\link{convpow}} for computing convolutions and convolution powers.
#' 
#' 
#' @seealso
#' \code{\link[stats:stats-package]{stats}}
#' 
NULL

# vim: tw=80 ft=r
